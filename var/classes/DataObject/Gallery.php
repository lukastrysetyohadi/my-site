<?php 

/** 
* Generated at: 2018-03-06T04:59:37+01:00
* Inheritance: no
* Variants: no
* Changed by: Lukas (2)
* IP: 127.0.0.1


Fields Summary: 
- title [input]
- image [image]
*/ 

namespace Pimcore\Model\DataObject;



/**
* @method static \Pimcore\Model\DataObject\Gallery\Listing getByTitle ($value, $limit = 0) 
* @method static \Pimcore\Model\DataObject\Gallery\Listing getByImage ($value, $limit = 0) 
*/

class Gallery extends Concrete {

public $o_classId = 1;
public $o_className = "Gallery";
public $title;
public $image;


/**
* @param array $values
* @return \Pimcore\Model\DataObject\Gallery
*/
public static function create($values = array()) {
	$object = new static();
	$object->setValues($values);
	return $object;
}

/**
* Get title - Title
* @return string
*/
public function getTitle () {
	$preValue = $this->preGetValue("title"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->title;
	return $data;
}

/**
* Set title - Title
* @param string $title
* @return \Pimcore\Model\DataObject\Gallery
*/
public function setTitle ($title) {
	$this->title = $title;
	return $this;
}

/**
* Get image - Image
* @return \Pimcore\Model\Asset\Image
*/
public function getImage () {
	$preValue = $this->preGetValue("image"); 
	if($preValue !== null && !\Pimcore::inAdmin()) { 
		return $preValue;
	}
	$data = $this->image;
	return $data;
}

/**
* Set image - Image
* @param \Pimcore\Model\Asset\Image $image
* @return \Pimcore\Model\DataObject\Gallery
*/
public function setImage ($image) {
	$this->image = $image;
	return $this;
}

protected static $_relationFields = array (
);

public $lazyLoadedFields = array (
);

}

