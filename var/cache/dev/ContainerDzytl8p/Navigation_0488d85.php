<?php

class Navigation_0488d85 extends \Pimcore\Templating\Helper\Navigation implements \ProxyManager\Proxy\VirtualProxyInterface
{

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $valueHolder5a9e1eff2cdcd215694733 = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializer5a9e1eff2cdf3410433093 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties5a9e1eff2cd52706535908 = array(
        
    );

    /**
     * {@inheritDoc}
     */
    public function getName()
    {
        $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, 'getName', array(), $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;

        return $this->valueHolder5a9e1eff2cdcd215694733->getName();
    }

    /**
     * {@inheritDoc}
     */
    public function buildNavigation(\Pimcore\Model\Document $activeDocument, \Pimcore\Model\Document $navigationRootDocument = null, string $htmlMenuPrefix = null, callable $pageCallback = null, $cache = true) : \Pimcore\Navigation\Container
    {
        $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, 'buildNavigation', array('activeDocument' => $activeDocument, 'navigationRootDocument' => $navigationRootDocument, 'htmlMenuPrefix' => $htmlMenuPrefix, 'pageCallback' => $pageCallback, 'cache' => $cache), $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;

        return $this->valueHolder5a9e1eff2cdcd215694733->buildNavigation($activeDocument, $navigationRootDocument, $htmlMenuPrefix, $pageCallback, $cache);
    }

    /**
     * {@inheritDoc}
     */
    public function getRenderer(string $alias) : \Pimcore\Navigation\Renderer\RendererInterface
    {
        $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, 'getRenderer', array('alias' => $alias), $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;

        return $this->valueHolder5a9e1eff2cdcd215694733->getRenderer($alias);
    }

    /**
     * {@inheritDoc}
     */
    public function render(\Pimcore\Navigation\Container $container, string $rendererName = 'menu', string $renderMethod = 'render', ... $rendererArguments)
    {
        $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, 'render', array('container' => $container, 'rendererName' => $rendererName, 'renderMethod' => $renderMethod, 'rendererArguments' => $rendererArguments), $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;

        return $this->valueHolder5a9e1eff2cdcd215694733->render($container, $rendererName, $renderMethod, ...$rendererArguments);
    }

    /**
     * {@inheritDoc}
     */
    public function __call($method, array $arguments = array()) : \Pimcore\Navigation\Renderer\RendererInterface
    {
        $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, '__call', array('method' => $method, 'arguments' => $arguments), $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;

        return $this->valueHolder5a9e1eff2cdcd215694733->__call($method, $arguments);
    }

    /**
     * {@inheritDoc}
     */
    public function setCharset($charset)
    {
        $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, 'setCharset', array('charset' => $charset), $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;

        return $this->valueHolder5a9e1eff2cdcd215694733->setCharset($charset);
    }

    /**
     * {@inheritDoc}
     */
    public function getCharset()
    {
        $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, 'getCharset', array(), $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;

        return $this->valueHolder5a9e1eff2cdcd215694733->getCharset();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?: $reflection = new \ReflectionClass(__CLASS__);
        $instance = (new \ReflectionClass(get_class()))->newInstanceWithoutConstructor();

        unset($instance->charset);

        \Closure::bind(function (\Pimcore\Templating\Helper\Navigation $instance) {
            unset($instance->builder, $instance->rendererLocator);
        }, $instance, 'Pimcore\\Templating\\Helper\\Navigation')->__invoke($instance);

        $instance->initializer5a9e1eff2cdf3410433093 = $initializer;

        return $instance;
    }

    /**
     * {@inheritDoc}
     */
    public function __construct(\Pimcore\Navigation\Builder $builder, \Psr\Container\ContainerInterface $rendererLocator)
    {
        static $reflection;

        if (! $this->valueHolder5a9e1eff2cdcd215694733) {
            $reflection = $reflection ?: new \ReflectionClass('Pimcore\\Templating\\Helper\\Navigation');
            $this->valueHolder5a9e1eff2cdcd215694733 = $reflection->newInstanceWithoutConstructor();
        unset($this->charset);

        \Closure::bind(function (\Pimcore\Templating\Helper\Navigation $instance) {
            unset($instance->builder, $instance->rendererLocator);
        }, $this, 'Pimcore\\Templating\\Helper\\Navigation')->__invoke($this);

        }

        $this->valueHolder5a9e1eff2cdcd215694733->__construct($builder, $rendererLocator);
    }

    /**
     * @param string $name
     */
    public function & __get($name)
    {
        $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, '__get', ['name' => $name], $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;

        if (isset(self::$publicProperties5a9e1eff2cd52706535908[$name])) {
            return $this->valueHolder5a9e1eff2cdcd215694733->$name;
        }

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5a9e1eff2cdcd215694733;

            $backtrace = debug_backtrace(false);
            trigger_error('Undefined property: ' . get_parent_class($this) . '::$' . $name . ' in ' . $backtrace[0]['file'] . ' on line ' . $backtrace[0]['line'], \E_USER_NOTICE);
            return $targetObject->$name;
            return;
        }

        $targetObject = $this->valueHolder5a9e1eff2cdcd215694733;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5a9e1eff2cdcd215694733;

            return $targetObject->$name = $value;
            return;
        }

        $targetObject = $this->valueHolder5a9e1eff2cdcd215694733;
        $accessor = function & () use ($targetObject, $name, $value) {
            return $targetObject->$name = $value;
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __isset($name)
    {
        $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, '__isset', array('name' => $name), $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5a9e1eff2cdcd215694733;

            return isset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder5a9e1eff2cdcd215694733;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    /**
     * @param string $name
     */
    public function __unset($name)
    {
        $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, '__unset', array('name' => $name), $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;

        $realInstanceReflection = new \ReflectionClass(get_parent_class($this));

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder5a9e1eff2cdcd215694733;

            unset($targetObject->$name);
            return;
        }

        $targetObject = $this->valueHolder5a9e1eff2cdcd215694733;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
        };
            $backtrace = debug_backtrace(true);
            $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
            $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __clone()
    {
        $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, '__clone', array(), $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;

        $this->valueHolder5a9e1eff2cdcd215694733 = clone $this->valueHolder5a9e1eff2cdcd215694733;
    }

    public function __sleep()
    {
        $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, '__sleep', array(), $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;

        return array('valueHolder5a9e1eff2cdcd215694733');
    }

    public function __wakeup()
    {
        unset($this->charset);

        \Closure::bind(function (\Pimcore\Templating\Helper\Navigation $instance) {
            unset($instance->builder, $instance->rendererLocator);
        }, $this, 'Pimcore\\Templating\\Helper\\Navigation')->__invoke($this);
    }

    /**
     * {@inheritDoc}
     */
    public function setProxyInitializer(\Closure $initializer = null)
    {
        $this->initializer5a9e1eff2cdf3410433093 = $initializer;
    }

    /**
     * {@inheritDoc}
     */
    public function getProxyInitializer()
    {
        return $this->initializer5a9e1eff2cdf3410433093;
    }

    /**
     * {@inheritDoc}
     */
    public function initializeProxy() : bool
    {
        return $this->initializer5a9e1eff2cdf3410433093 && ($this->initializer5a9e1eff2cdf3410433093->__invoke($valueHolder5a9e1eff2cdcd215694733, $this, 'initializeProxy', array(), $this->initializer5a9e1eff2cdf3410433093) || 1) && $this->valueHolder5a9e1eff2cdcd215694733 = $valueHolder5a9e1eff2cdcd215694733;
    }

    /**
     * {@inheritDoc}
     */
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder5a9e1eff2cdcd215694733;
    }

    /**
     * {@inheritDoc}
     */
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder5a9e1eff2cdcd215694733;
    }


}
