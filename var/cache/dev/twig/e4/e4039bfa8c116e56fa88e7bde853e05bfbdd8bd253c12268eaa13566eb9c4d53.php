<?php

/* @WebProfiler/Collector/ajax.html.twig */
class __TwigTemplate_bb1dac215236be84b6ac8434b6a0d4d730be7346810a29eb5dd436b65d3ee8d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_141ba255f169788bd5d480a69cf5666c5a78f356c7b48ff5803dc46478ef6c59 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_141ba255f169788bd5d480a69cf5666c5a78f356c7b48ff5803dc46478ef6c59->enter($__internal_141ba255f169788bd5d480a69cf5666c5a78f356c7b48ff5803dc46478ef6c59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $__internal_dc6a630662c6ee0d01d74bc7380a9d4d2bf72a7371bf81bb5b6718231d2d95e8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dc6a630662c6ee0d01d74bc7380a9d4d2bf72a7371bf81bb5b6718231d2d95e8->enter($__internal_dc6a630662c6ee0d01d74bc7380a9d4d2bf72a7371bf81bb5b6718231d2d95e8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_141ba255f169788bd5d480a69cf5666c5a78f356c7b48ff5803dc46478ef6c59->leave($__internal_141ba255f169788bd5d480a69cf5666c5a78f356c7b48ff5803dc46478ef6c59_prof);

        
        $__internal_dc6a630662c6ee0d01d74bc7380a9d4d2bf72a7371bf81bb5b6718231d2d95e8->leave($__internal_dc6a630662c6ee0d01d74bc7380a9d4d2bf72a7371bf81bb5b6718231d2d95e8_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_6762981be893a01cb4eb16d1cc315429a05133385a3cfe38c81ba4762b1fdf21 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6762981be893a01cb4eb16d1cc315429a05133385a3cfe38c81ba4762b1fdf21->enter($__internal_6762981be893a01cb4eb16d1cc315429a05133385a3cfe38c81ba4762b1fdf21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_907418caa85310c136989f997e3b6b9bcae99c823dc5ce8d7bc815cc55582c7e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_907418caa85310c136989f997e3b6b9bcae99c823dc5ce8d7bc815cc55582c7e->enter($__internal_907418caa85310c136989f997e3b6b9bcae99c823dc5ce8d7bc815cc55582c7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_907418caa85310c136989f997e3b6b9bcae99c823dc5ce8d7bc815cc55582c7e->leave($__internal_907418caa85310c136989f997e3b6b9bcae99c823dc5ce8d7bc815cc55582c7e_prof);

        
        $__internal_6762981be893a01cb4eb16d1cc315429a05133385a3cfe38c81ba4762b1fdf21->leave($__internal_6762981be893a01cb4eb16d1cc315429a05133385a3cfe38c81ba4762b1fdf21_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "@WebProfiler/Collector/ajax.html.twig", "D:\\PROJECT\\SIMPLY PROJECT\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\ajax.html.twig");
    }
}
