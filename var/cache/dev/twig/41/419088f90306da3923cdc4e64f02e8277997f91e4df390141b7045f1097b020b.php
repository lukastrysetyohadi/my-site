<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_53ec95b01f679558032bf8b2595ab1c6821ec279fc93f5d17acf00559c1f42be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_33791f1c8fcbd6bb6ffa17c4814beb29ef3754a29c0b91afef8ab03efac21b8f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_33791f1c8fcbd6bb6ffa17c4814beb29ef3754a29c0b91afef8ab03efac21b8f->enter($__internal_33791f1c8fcbd6bb6ffa17c4814beb29ef3754a29c0b91afef8ab03efac21b8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $__internal_5ef6a650d07f98196fe77ff7c0d9a00f6a856931f5cf5507dc98d85c966753e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ef6a650d07f98196fe77ff7c0d9a00f6a856931f5cf5507dc98d85c966753e1->enter($__internal_5ef6a650d07f98196fe77ff7c0d9a00f6a856931f5cf5507dc98d85c966753e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_33791f1c8fcbd6bb6ffa17c4814beb29ef3754a29c0b91afef8ab03efac21b8f->leave($__internal_33791f1c8fcbd6bb6ffa17c4814beb29ef3754a29c0b91afef8ab03efac21b8f_prof);

        
        $__internal_5ef6a650d07f98196fe77ff7c0d9a00f6a856931f5cf5507dc98d85c966753e1->leave($__internal_5ef6a650d07f98196fe77ff7c0d9a00f6a856931f5cf5507dc98d85c966753e1_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_3cad64bc9e3277a4f57742db2de1cdd103ef20a7f23aed44e9d77fce4d9bfcae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3cad64bc9e3277a4f57742db2de1cdd103ef20a7f23aed44e9d77fce4d9bfcae->enter($__internal_3cad64bc9e3277a4f57742db2de1cdd103ef20a7f23aed44e9d77fce4d9bfcae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_75a85e5df2816be6aefd4cd49c11eb892d9802ea7b42ec3d0b53b3720b3d48ff = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_75a85e5df2816be6aefd4cd49c11eb892d9802ea7b42ec3d0b53b3720b3d48ff->enter($__internal_75a85e5df2816be6aefd4cd49c11eb892d9802ea7b42ec3d0b53b3720b3d48ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_75a85e5df2816be6aefd4cd49c11eb892d9802ea7b42ec3d0b53b3720b3d48ff->leave($__internal_75a85e5df2816be6aefd4cd49c11eb892d9802ea7b42ec3d0b53b3720b3d48ff_prof);

        
        $__internal_3cad64bc9e3277a4f57742db2de1cdd103ef20a7f23aed44e9d77fce4d9bfcae->leave($__internal_3cad64bc9e3277a4f57742db2de1cdd103ef20a7f23aed44e9d77fce4d9bfcae_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_6ef7be55e181850798564d6387ec4738661490589fea41a46ac53780a80c4a58 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6ef7be55e181850798564d6387ec4738661490589fea41a46ac53780a80c4a58->enter($__internal_6ef7be55e181850798564d6387ec4738661490589fea41a46ac53780a80c4a58_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_29b392ddec2f845e0eae986282512cd03c6170b3d05846b039ec3d619968cdbf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29b392ddec2f845e0eae986282512cd03c6170b3d05846b039ec3d619968cdbf->enter($__internal_29b392ddec2f845e0eae986282512cd03c6170b3d05846b039ec3d619968cdbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["exception"]) || array_key_exists("exception", $context) ? $context["exception"] : (function () { throw new Twig_Error_Runtime('Variable "exception" does not exist.', 137, $this->getSourceContext()); })()), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) || array_key_exists("status_code", $context) ? $context["status_code"] : (function () { throw new Twig_Error_Runtime('Variable "status_code" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) || array_key_exists("status_text", $context) ? $context["status_text"] : (function () { throw new Twig_Error_Runtime('Variable "status_text" does not exist.', 137, $this->getSourceContext()); })()), "html", null, true);
        echo ")
";
        
        $__internal_29b392ddec2f845e0eae986282512cd03c6170b3d05846b039ec3d619968cdbf->leave($__internal_29b392ddec2f845e0eae986282512cd03c6170b3d05846b039ec3d619968cdbf_prof);

        
        $__internal_6ef7be55e181850798564d6387ec4738661490589fea41a46ac53780a80c4a58->leave($__internal_6ef7be55e181850798564d6387ec4738661490589fea41a46ac53780a80c4a58_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_b526130c4d2c0922b99f1d676084bec7d69c3995f601891c457f462f2b914fea = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b526130c4d2c0922b99f1d676084bec7d69c3995f601891c457f462f2b914fea->enter($__internal_b526130c4d2c0922b99f1d676084bec7d69c3995f601891c457f462f2b914fea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_b68168a48be0cd21fab513ac0e61b1dd3f6b2a4eea966f2ca3c108e5e26eab0a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b68168a48be0cd21fab513ac0e61b1dd3f6b2a4eea966f2ca3c108e5e26eab0a->enter($__internal_b68168a48be0cd21fab513ac0e61b1dd3f6b2a4eea966f2ca3c108e5e26eab0a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 141)->display($context);
        
        $__internal_b68168a48be0cd21fab513ac0e61b1dd3f6b2a4eea966f2ca3c108e5e26eab0a->leave($__internal_b68168a48be0cd21fab513ac0e61b1dd3f6b2a4eea966f2ca3c108e5e26eab0a_prof);

        
        $__internal_b526130c4d2c0922b99f1d676084bec7d69c3995f601891c457f462f2b914fea->leave($__internal_b526130c4d2c0922b99f1d676084bec7d69c3995f601891c457f462f2b914fea_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "D:\\PROJECT\\SIMPLY PROJECT\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
