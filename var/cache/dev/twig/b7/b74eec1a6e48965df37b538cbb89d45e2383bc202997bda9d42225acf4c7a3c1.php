<?php

/* @Twig/layout.html.twig */
class __TwigTemplate_e88152ff8fe01676da50b0639f285f07d35dca2dd765ec2a56fbf1dafa1a8ef9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e48e97cfdd6e73a3cc3bc6842b230d8a142e9b263c68208ffa632921eb479690 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e48e97cfdd6e73a3cc3bc6842b230d8a142e9b263c68208ffa632921eb479690->enter($__internal_e48e97cfdd6e73a3cc3bc6842b230d8a142e9b263c68208ffa632921eb479690_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        $__internal_7b6fdd884e5739a4ab03180e87b4615f1cb6ca1d7a84799139e34f3cf62fb333 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b6fdd884e5739a4ab03180e87b4615f1cb6ca1d7a84799139e34f3cf62fb333->enter($__internal_7b6fdd884e5739a4ab03180e87b4615f1cb6ca1d7a84799139e34f3cf62fb333_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_e48e97cfdd6e73a3cc3bc6842b230d8a142e9b263c68208ffa632921eb479690->leave($__internal_e48e97cfdd6e73a3cc3bc6842b230d8a142e9b263c68208ffa632921eb479690_prof);

        
        $__internal_7b6fdd884e5739a4ab03180e87b4615f1cb6ca1d7a84799139e34f3cf62fb333->leave($__internal_7b6fdd884e5739a4ab03180e87b4615f1cb6ca1d7a84799139e34f3cf62fb333_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_ffa942416d72a03a08eb425de9f40aae500293fbf80a7cd2747c917658d3af68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ffa942416d72a03a08eb425de9f40aae500293fbf80a7cd2747c917658d3af68->enter($__internal_ffa942416d72a03a08eb425de9f40aae500293fbf80a7cd2747c917658d3af68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_1f474f771936ddc9b4d79feeb00fb1ac68fb9146de1514c91db4eda9186da321 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f474f771936ddc9b4d79feeb00fb1ac68fb9146de1514c91db4eda9186da321->enter($__internal_1f474f771936ddc9b4d79feeb00fb1ac68fb9146de1514c91db4eda9186da321_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_1f474f771936ddc9b4d79feeb00fb1ac68fb9146de1514c91db4eda9186da321->leave($__internal_1f474f771936ddc9b4d79feeb00fb1ac68fb9146de1514c91db4eda9186da321_prof);

        
        $__internal_ffa942416d72a03a08eb425de9f40aae500293fbf80a7cd2747c917658d3af68->leave($__internal_ffa942416d72a03a08eb425de9f40aae500293fbf80a7cd2747c917658d3af68_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_d1c810faa7804a743a46a2ed9579a60b1b7c5accb75e3efe49fce658d800e28f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1c810faa7804a743a46a2ed9579a60b1b7c5accb75e3efe49fce658d800e28f->enter($__internal_d1c810faa7804a743a46a2ed9579a60b1b7c5accb75e3efe49fce658d800e28f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_1341dfeda9ca547b8285b44ef27d90b7823e3e2b83a6d5f1c65da428dce1c17a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1341dfeda9ca547b8285b44ef27d90b7823e3e2b83a6d5f1c65da428dce1c17a->enter($__internal_1341dfeda9ca547b8285b44ef27d90b7823e3e2b83a6d5f1c65da428dce1c17a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_1341dfeda9ca547b8285b44ef27d90b7823e3e2b83a6d5f1c65da428dce1c17a->leave($__internal_1341dfeda9ca547b8285b44ef27d90b7823e3e2b83a6d5f1c65da428dce1c17a_prof);

        
        $__internal_d1c810faa7804a743a46a2ed9579a60b1b7c5accb75e3efe49fce658d800e28f->leave($__internal_d1c810faa7804a743a46a2ed9579a60b1b7c5accb75e3efe49fce658d800e28f_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_f8c19ce14dfdd19e49bfeb90708145133b65c18a8caca3fa9bdfe72179e6f559 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f8c19ce14dfdd19e49bfeb90708145133b65c18a8caca3fa9bdfe72179e6f559->enter($__internal_f8c19ce14dfdd19e49bfeb90708145133b65c18a8caca3fa9bdfe72179e6f559_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_eff238f60315385b4451a7cf169027501fd565eef49d3f289a02edd7f2ced6ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eff238f60315385b4451a7cf169027501fd565eef49d3f289a02edd7f2ced6ae->enter($__internal_eff238f60315385b4451a7cf169027501fd565eef49d3f289a02edd7f2ced6ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_eff238f60315385b4451a7cf169027501fd565eef49d3f289a02edd7f2ced6ae->leave($__internal_eff238f60315385b4451a7cf169027501fd565eef49d3f289a02edd7f2ced6ae_prof);

        
        $__internal_f8c19ce14dfdd19e49bfeb90708145133b65c18a8caca3fa9bdfe72179e6f559->leave($__internal_f8c19ce14dfdd19e49bfeb90708145133b65c18a8caca3fa9bdfe72179e6f559_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "@Twig/layout.html.twig", "D:\\PROJECT\\SIMPLY PROJECT\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\layout.html.twig");
    }
}
