<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_b41637bcab85599d7b93bd2579e06fdc4b2c567544695bda80fe1a47b9ea10ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a200589dbfc9ab147e79fa32f87d0c4b0112829ace2a9195304ae9364f5375fa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a200589dbfc9ab147e79fa32f87d0c4b0112829ace2a9195304ae9364f5375fa->enter($__internal_a200589dbfc9ab147e79fa32f87d0c4b0112829ace2a9195304ae9364f5375fa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $__internal_d59a2f8bb976f8beb8eac365b4575af319736df7158f505fc165c9e29f945e11 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d59a2f8bb976f8beb8eac365b4575af319736df7158f505fc165c9e29f945e11->enter($__internal_d59a2f8bb976f8beb8eac365b4575af319736df7158f505fc165c9e29f945e11_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a200589dbfc9ab147e79fa32f87d0c4b0112829ace2a9195304ae9364f5375fa->leave($__internal_a200589dbfc9ab147e79fa32f87d0c4b0112829ace2a9195304ae9364f5375fa_prof);

        
        $__internal_d59a2f8bb976f8beb8eac365b4575af319736df7158f505fc165c9e29f945e11->leave($__internal_d59a2f8bb976f8beb8eac365b4575af319736df7158f505fc165c9e29f945e11_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_92919737ae68bc776cb99ddd373fc51fb719129ea4b16b0c48e99cbf6f89f659 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_92919737ae68bc776cb99ddd373fc51fb719129ea4b16b0c48e99cbf6f89f659->enter($__internal_92919737ae68bc776cb99ddd373fc51fb719129ea4b16b0c48e99cbf6f89f659_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_f3a6835f7e78f3a12b5684caa5686105562f5787921dd22115f37977b37167ad = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3a6835f7e78f3a12b5684caa5686105562f5787921dd22115f37977b37167ad->enter($__internal_f3a6835f7e78f3a12b5684caa5686105562f5787921dd22115f37977b37167ad_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_f3a6835f7e78f3a12b5684caa5686105562f5787921dd22115f37977b37167ad->leave($__internal_f3a6835f7e78f3a12b5684caa5686105562f5787921dd22115f37977b37167ad_prof);

        
        $__internal_92919737ae68bc776cb99ddd373fc51fb719129ea4b16b0c48e99cbf6f89f659->leave($__internal_92919737ae68bc776cb99ddd373fc51fb719129ea4b16b0c48e99cbf6f89f659_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_aa0ea5f3c9dd4e74b4af7b7a6db32463648d14a629701f56954f3921a82df84e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa0ea5f3c9dd4e74b4af7b7a6db32463648d14a629701f56954f3921a82df84e->enter($__internal_aa0ea5f3c9dd4e74b4af7b7a6db32463648d14a629701f56954f3921a82df84e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_e5d0e5eca2da0caf1a88d012bb98f7c5dbde6c4ffe82cb019ef78af80934a262 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e5d0e5eca2da0caf1a88d012bb98f7c5dbde6c4ffe82cb019ef78af80934a262->enter($__internal_e5d0e5eca2da0caf1a88d012bb98f7c5dbde6c4ffe82cb019ef78af80934a262_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_e5d0e5eca2da0caf1a88d012bb98f7c5dbde6c4ffe82cb019ef78af80934a262->leave($__internal_e5d0e5eca2da0caf1a88d012bb98f7c5dbde6c4ffe82cb019ef78af80934a262_prof);

        
        $__internal_aa0ea5f3c9dd4e74b4af7b7a6db32463648d14a629701f56954f3921a82df84e->leave($__internal_aa0ea5f3c9dd4e74b4af7b7a6db32463648d14a629701f56954f3921a82df84e_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_d9af78658d0060fa9055717a091214f06c74c0001086e4744372a44f68de8d4b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d9af78658d0060fa9055717a091214f06c74c0001086e4744372a44f68de8d4b->enter($__internal_d9af78658d0060fa9055717a091214f06c74c0001086e4744372a44f68de8d4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_4cd5d1240f69249b372ec38d7ee7d1ec310684784ad0fa3e63bec335b84015bb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4cd5d1240f69249b372ec38d7ee7d1ec310684784ad0fa3e63bec335b84015bb->enter($__internal_4cd5d1240f69249b372ec38d7ee7d1ec310684784ad0fa3e63bec335b84015bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) || array_key_exists("token", $context) ? $context["token"] : (function () { throw new Twig_Error_Runtime('Variable "token" does not exist.', 13, $this->getSourceContext()); })()))));
        echo "
";
        
        $__internal_4cd5d1240f69249b372ec38d7ee7d1ec310684784ad0fa3e63bec335b84015bb->leave($__internal_4cd5d1240f69249b372ec38d7ee7d1ec310684784ad0fa3e63bec335b84015bb_prof);

        
        $__internal_d9af78658d0060fa9055717a091214f06c74c0001086e4744372a44f68de8d4b->leave($__internal_d9af78658d0060fa9055717a091214f06c74c0001086e4744372a44f68de8d4b_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  85 => 12,  71 => 7,  68 => 6,  59 => 5,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "D:\\PROJECT\\SIMPLY PROJECT\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
